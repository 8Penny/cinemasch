﻿# README #
![](https://bitbucket.org/repo/8zXAEg7/images/3323115624-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B911.png)


### Удобный поиск любых сеансов фильмов в Санкт-Петербурге ###

* Фильтрация по ценам, фильму, дате
* Визуализация сеансов на карте
* Статистика цен по времени

### В вам понадобятся следующие библиотеки: ###

* requests
* plotly
* beautifulsoup4
* celery
* Django
* django-celery
* djangorestframework
* redis
* html5lib

**Так же вам понадобится Redis.**

### Установка: ###

* Распакуйте CinemaSch

* Запустите сервер Redis (Port:6379)

* Создайте суперюзера, введя в командной строке (cd path\CinemaSch\cinema_schedule && python manage.py creatsuperuser)

* Откройте 3 командных строки и во всех перейдите в директорию (cd path\CinemaSch\cinema_schedule)

* Далее в каждой введите соотвественно:

  1. python manage.py runserver

  1. python manage.py celery beat

  1. python manage.py celery worker

* Перейдите по ссылке [http://{host}/admin/djcelery/periodictask/](http://127.0.0.1:8000/admin/djcelery/periodictask/)


* измените SessionPeriodicTask (на первый раз выберете текущее время, потом измените его обратно на 00:01)
![Безымянный.png](https://bitbucket.org/repo/8zXAEg7/images/1997951708-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
сохраните

** Парсинг и обработка данных занимает около 10-15 минут. В первый раз не нужно сразу начинать работу с сайтом, дождитесь окончания парсинга. В последующие дни при регулярной работе сервера, к сайту можно будет обращаться в любое время.**

* домашняя страница находится по этой [ссылке](http://127.0.0.1:8000/spbcinemasearch/)