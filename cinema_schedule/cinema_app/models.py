from django.db import models

# Create your models here.
class CinemaList(models.Model):
    name = models.CharField(max_length=200, unique=True)
    adress = models.CharField(max_length=300, unique=True)
    lon = models.FloatField(blank = True, null = True)
    lat = models.FloatField(blank = True, null = True)


    def __str__(self):
        return self.name


class FilmList(models.Model):
    name = models.CharField(max_length=200, unique = True)
    img = models.URLField()

    def __str__(self):
        return "{}".format(self.name)

class Schedule(models.Model):
    cinema = models.ForeignKey(CinemaList, related_name='cinemas', on_delete=models.CASCADE)
    film = models.ForeignKey(FilmList, related_name='films', on_delete=models.CASCADE)
    date = models.DateField(null=False, blank=False)
    time = models.TimeField(null=False, blank=False)
    price = models.IntegerField(null = True, blank = True)

    class Meta:
        unique_together = ('cinema','film','date','time','price')

    def __str__(self):
        return "{}".format(self.cinema,self.film,self.date,self.time,self.price)

class StatisticDay(models.Model):
    date = models.DateField(null=False, blank=False, unique=True)
    avg_price = models.IntegerField(null = False, blank = False)

    def __str__(self):
        return "{}".format(self.date,self.avg_price)

class StatisticTime(models.Model):
    time = models.TimeField(null=False, blank=False)
    avg_price = models.IntegerField(null = False, blank = False)

    def __str__(self):
        return "{}".format(self.time,self.avg_price)
