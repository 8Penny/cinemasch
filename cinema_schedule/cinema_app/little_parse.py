import requests
import re
from bs4 import BeautifulSoup
import time
from datetime import date


def l_parse():
    url = 'https://spb.kinoafisha.info/cinema/'
    r = requests.get(url)
    page = BeautifulSoup(r.text, "html5lib")
    tbl_list = page.find('div', attrs={'class': 'aboutFav_columns'})
    names = tbl_list.findAll('a', attrs={'class': 'theater_name link link-default'})
    data_list = []
    cinema_list = []
    film_list = []
    cou = 0
    for i in names:
        '''if cou>30:
            break'''
        if i.get('href').find('//spb.') != -1:

            r = requests.get('https:{}'.format(i.get('href')))
            page = BeautifulSoup(r.text, "html5lib")
            cou += 1
            try:

                tbl_list = page.find('div', attrs={'class': 'week week-view1'})
                address = page.find('a', attrs={'class': 'theaterInfo_addr link link-default'}).find('span').string
                hrefs = tbl_list.findAll('a')

                for href in hrefs:

                    day_url = href.get('href')
                    _date = day_url.split('=')[1].split('&')[0]
                    _date = date(int(_date[0:4]), int(_date[4:6]), int(_date[6:]))

                    '''
                    if _date!=date.today():
                        break'''

                    r = requests.get('https:{}'.format(day_url))
                    page_1 = BeautifulSoup(r.text, "html5lib")
                    tbl_list = page_1.find('div', attrs={'class': 'showtimes_frame'})
                    name_time_price = tbl_list.findAll('div', attrs={'data-sub-modules': 'Fav'})
                    for film in name_time_price:

                        name = film.find('span', attrs={'class': 'link_border'}).string
                        time = film.findAll('span', attrs={'class': 'session2_time'})
                        price = film.findAll('span', attrs={'class': 'session2_price'})
                        img = film.find('img', attrs={'class': 'films_icon'}).get('src')
                        if not film_list.count([name, img]) and name:
                            film_list.append([name, img])

                        for count in range(0, len(time)):
                            data = [i.string, name, _date, time[count].string, price[count].string]
                            data_list.append(data)

            except:
                data = [i.string, None, None, None, None]

            data_2 = [i.string, address[:-1]]
            cinema_list.append(data_2)

    finish_data_list = []
    for i in data_list:
        if i not in finish_data_list:
            finish_data_list.append(i)


    return finish_data_list, cinema_list, film_list


if __name__ == '__main__':
    l_parse()

