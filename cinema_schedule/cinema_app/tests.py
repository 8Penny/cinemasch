from django.test import TestCase
from .models import *
from datetime import datetime, date, time

class ModelTestCase(TestCase):
    """This class defines the test suite for the bucketlist model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.cinema_name = "My cinema"
        self.cinema_adress = "Adres, 14k - 45"
        self.cin = CinemaList(name=self.cinema_name, adress = self.cinema_adress)

        self.film_name = "My film"
        self.film_url = 'https://pp.userapi.com/c636430/v636430278/4c969/hxiX1cVf3Ic.jpg'
        self.fil = FilmList(name = self.film_name, img = self.film_url)

        '''self.cinema = "My cinema"
        self.film = "My film"'''
        self.date = date(2017,5,13)
        self.time = time(12, 30)
        self.price = 500
        self.sch = Schedule(cinema_id=1, film_id=1, date = self.date, time = self.time, price = self.price)

        self.time = time(9,0,0)
        self.avg_price=233
        self.avg_pr = StatisticTime(time = self.time, avg_price= self.avg_price)

        self.date = date(2017, 5, 17)
        self.avg_price2 = 233
        self.avg_pr_d = StatisticDay(datee=self.date, avg_price2=self.avg_price)


    def test_model_can_create_a_cinema(self):
        """Test the bucketlist model can create a bucketlist."""
        old_count = CinemaList.objects.count()
        self.cin.save()
        new_count = CinemaList.objects.count()
        self.assertNotEqual(old_count, new_count)


    def test_model_can_create_a_film(self):
        old_count = FilmList.objects.count()
        self.fil.save()
        new_count = FilmList.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_create_a_chedule(self):
        old_count = Schedule.objects.count()
        self.sch.save()
        new_count = Schedule.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_create_a_avg_price_time(self):
        old_count = StatisticTime.objects.count()
        self.avg_pr.save()
        new_count = StatisticTime.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_create_a_avg_price_day(self):
        old_count = StatisticDay.objects.count()
        self.avg_pr_d.save()
        new_count = StatisticDay.objects.count()
        self.assertNotEqual(old_count, new_count)

