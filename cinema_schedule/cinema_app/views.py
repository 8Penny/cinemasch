from django.shortcuts import render
from datetime import *
from .forms import *
from django.http import HttpResponse
from django.template.loader import get_template

import plotly.offline as opy
import plotly.graph_objs as go
import datetime


def built_list1(filmlist):  # функция для построения списка в формате для leaflet

    di = {}  # словарь для группировки по кинотеатрам
    for i in filmlist:
        try:
            a = di[i["cinema_id"]]
            if type(a) != list:
                a = [a]
            a.append({"date": i["date"], "time": i["time"], "price": i["price"]})
            a = sorted(a, key=lambda k: k['time'])
            di.update({i["cinema_id"]: a})
        except:
            di.update({i["cinema_id"]: {"date": i["date"], "time": i["time"], "price": i["price"]}})

    keys = di.keys()
    result = []  # [[имякинотеатра, широта, долгота, расписание],[...]]

    for key in keys:
        cd = CinemaList.objects.get(id=key)
        sessions = []
        if type(di[key]) == list:
            for cinema in di[key]:
                sessions.append("{0} - {1} р".format(str(cinema["time"])[:-3], cinema["price"]))
        else:
            sessions.append("{0} - {1} р".format(str(di[key]["time"])[:-3], di[key]["price"]))
        a = [cd.name, cd.lat, cd.lon, sessions, cd.adress]
        result.append(a)

    c = {
        'filmlist': result
    }
    return c


def statistic_t():  # статистика цен по времени

    querys = StatisticTime.objects.all().order_by("time")
    prices = querys.values_list("time", "avg_price")
    try:
        querys2 = StatisticDay.objects.filter(date=date.today())
    except:
        querys2 = StatisticDay.objects.filter(date=date.today()-timedelta(days=1))
    avg_day_price = querys2.values_list("avg_price")[0][0]
    x = []
    y = []

    for counter in range(0, len(prices)):
        if prices[counter][0] > datetime.time(6, 0, 0):
            prices = prices[counter:] + prices[:counter]
            break
    for i in prices:
        if i[1] < avg_day_price + 150:  # верхняя цена для статистики: средняя цена + 150
            y += [str(i[0])[:-3]]
            x += [i[1]]

    trace1 = go.Scatter(x=y, y=x, marker={"color": "rgb(255,255,255)", "symbol": 104, "size": "10"},
                        mode="lines", name="1st Trace")
    data = go.Data([trace1])
    layout = go.Layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor="rgba(0,0,0,0)",
                       xaxis={'showgrid': False},yaxis={'showgrid': False})
    figure = go.Figure(data=data, layout=layout)
    div = opy.plot(figure, auto_open=False, output_type="div")
    return div


def today_and_not_films():  # фильмы сегодня в прокате и остальные (в прокате)

    q = Schedule.objects.filter(date=date.today()).values_list("film_id").distinct()
    filmlists2 = list(q)
    film_ids_today = []
    for film_id in filmlists2:
        film_ids_today.append(film_id[0])
    q = FilmList.objects.filter(id__in=film_ids_today).values()
    f_list1 = list(q)
    
    q = Schedule.objects.values_list("film_id").distinct()
    filmlists2 = list(q)
    film_ids = []
    for film_id in filmlists2:
        film_ids.append(film_id[0])
    q = FilmList.objects.filter(id__in=film_ids).exclude(id__in=film_ids_today).values()
    f_list2 = list(q)

    return f_list1,f_list2


def built_list2(filmlist):

    a = []
    for i in filmlist:
        f_data = list(FilmList.objects.filter(id = i["film_id"]).values_list('name', 'img'))
        c_data = list(CinemaList.objects.filter(id = i["cinema_id"]).values_list('name'))
        a.append([f_data[0][0],f_data[0][1], c_data[0][0], i["time"], i["price"]]) #[имя фильма, постер, кинотеатр, время, цена]
        
    from itertools import groupby
    di = {}  # группировка по фильмам
    for key, group in groupby(a, lambda x: x[0]):
        for session in group:
            try:
                a = di[key]
                if type(a) != list:
                    a = [a]
                a.append(session)
                di.update({key:a})
            except:
                session = [session]
                di.update({key:session})
    T = []  # список с элементами вида [фильм, постер,[[кинотеатр,[[...,...,...,время,цена],...],...],...]

    for ikey in di.keys():
        a = di[ikey]
        di2 = {}  # группировка по кинотеатрам
        for key, group in groupby(a, lambda x: x[2]):
            for session in group:
                try:
                    b = di2[key]
                    if type(b) != list:
                        b = [b]

                    b.append(session)
                    di2.update({key: b})
                except:
                    session = [session]
                    di2.update({key: session})

        M = []
        for ckey in di2.keys():
            val = di2[ckey]
            if type(val) != list:
                val = [val]
            val.sort(key=lambda i: i[3])  # сортировка по времени
            m = [ckey, val]
            M.append(m)

        t = [ikey, a[0][1], M]  #
        T.append(t)

    c = {
        'filmlist': T
    }
    return c


def HomeAndSearch(request):

    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            input_pr1 = form.cleaned_data["price1"]
            input_pr2 = form.cleaned_data["price2"]
            input_date = form.cleaned_data["t_date"]
            input_film = form.cleaned_data["film"]
            print(input_date, input_pr1, input_pr2,input_film)

            if not input_pr1 and input_pr2:
                input_pr1 = 1

            if input_pr1 and input_pr2:

                if input_film != "0":
                    filmlist = list(Schedule.objects.filter(date=input_date,film_id=input_film,price__in = range(input_pr1,input_pr2+1)).values())
                    template = get_template("lists1.tmpl")
                    context = built_list1(filmlist)
                else:
                    filmlist = list(Schedule.objects.filter(date=input_date,price__in = range(input_pr1,input_pr2+1)).values())
                    context = built_list2(filmlist)
                    template = get_template("lists2.tmpl")
            else:
                if input_film != "0":
                    filmlist = list(Schedule.objects.filter(date=input_date, film_id=input_film).values())
                    template = get_template("lists1.tmpl")
                    context = built_list1(filmlist)
                else:
                    filmlist = list(Schedule.objects.filter(date=input_date).values())
                    context = built_list2(filmlist)
                    template = get_template("lists2.tmpl")

            if context == {'filmlist':[]}:
                context = {}

            return HttpResponse(template.render(context))

    else:
        form = SearchForm({"t_date":str(date.today()), "film": 0})
        div = statistic_t()
        f_list1,f_list2 = today_and_not_films()
        today_avg_pr = StatisticDay.objects.get(date = date.today()).avg_price
        print(today_avg_pr)

    return render(request, "home_page.tmpl", {"form": form, "graph": div, "f_list1": f_list1,
                                          "f_list2": f_list2, "avg_price": today_avg_pr})


def FilmSearch(request, film_id):

    template = get_template("lists1.tmpl")
    filmlist = list(Schedule.objects.filter(film_id=film_id, date=date.today()).values())
    c = built_list1(filmlist)

    return HttpResponse(template.render(c, request))



