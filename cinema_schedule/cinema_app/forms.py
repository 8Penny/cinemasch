from django import forms
from django.forms.extras.widgets import SelectDateWidget
from .models import *

class SearchForm(forms.Form):

    price1 = forms.IntegerField(required=False, min_value=0)
    price2 = forms.IntegerField(required=False, min_value=0)
    t_date = forms.DateField(widget=SelectDateWidget())

    queryset2 = Schedule.objects.values_list('film_id').distinct()
    filmlists2 = list(queryset2)
    film_ids = []
    for film_id in filmlists2:
        film_ids.append(film_id[0])
    queryset3 = FilmList.objects.filter(id__in=film_ids).values_list('id','name')
    f_list=list(queryset3)
    CHOICES = f_list+[(0, 'не выбран')]

    def sort_col(i):
        return i[1]

    CHOICES.sort(key=sort_col)

    film = forms.ChoiceField(choices=CHOICES, required=False)

