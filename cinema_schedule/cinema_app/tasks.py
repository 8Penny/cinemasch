# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os

from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cinema_schedule.settings')


from celery.task import task
import django
django.setup()

from .little_parse import *
from .models import *
from datetime import *


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def just_print():

    print('start')
    _date = date.today()
    one_day = timedelta(days=1)
    dates = set(list(Schedule.objects.values_list('date', flat=True)))
    sorted_dates = sorted(dates)

    for d in sorted_dates:
        if d < _date:
            sch = Schedule.objects.filter(date=d)
            sch.delete()

    data_list, cinema_list, film_list = l_parse()  # парсинг
    print('parse: OK')

    for film in film_list:
        obj = FilmList(name=film[0], img = film[1])
        try:
            obj.save()
        except:
            pass

    statisticdict = {}

    for data in data_list:

        try:
            q1 = CinemaList.objects.get(name=data[0]).pk
            q2 = FilmList.objects.get(name=data[1]).pk
            sch = Schedule(cinema_id=q1, film_id=q2, date=data[2], time=data[3], price=data[4])
            if data[2] == _date:

                try:
                    l_list =[int(data[4])]
                    if statisticdict.get(data[3], '-') is not '-':
                        l_list += statisticdict.get(data[3])

                    statisticdict[data[3]]=l_list
                    statisticdict.update({data[3]: l_list})
                except:
                    pass

            sch.save()
        except:
            pass


    try:
        StatisticTime.objects.all().delete()
    except:
        pass



    for i in list(statisticdict.keys()):
        try:
            avg_price_time = statisticdict.get(i)
            stat_t = StatisticTime(time=i, avg_price=sum(avg_price_time)/len(avg_price_time))
            stat_t.save()
        except:
            pass

    queryset = StatisticTime.objects.all()
    prices = queryset.values_list('avg_price')

    s = 0
    for j in prices:
        s += int(j[0])
    avg_price_day = s/len(prices)

    stat_d = StatisticDay(date=_date, avg_price=avg_price_day)
    try:
        stat_d.save()

    except:
        pass

    print('Finish')