
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *


urlpatterns = {
    url(r'^spbcinemasearch/$', HomeAndSearch, name="test"),
    url(r'spbcinemasearch/search/film(?P<film_id>[0-9]+)/$', FilmSearch, name='filmsearch'),

}

urlpatterns = format_suffix_patterns(urlpatterns)